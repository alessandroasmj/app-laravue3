export default {
    serverError: 'Ops! Algo de errado ocorreu.',
    LoginInvalidException: 'Ops! E-mail e/ou senha incorretos!',
    ResetPasswordTokenInvalidException: 'Token de reset inválido',
    UserHasBeenTakenException: 'Usuário já cadastrado.',
    VerifyEmailTokenInvalidException: 'Token de validação de email inválido.',
    UserNotFoundException: 'Usuário não cadastrado.',
};
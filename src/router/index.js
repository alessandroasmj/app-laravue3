import { createRouter, createWebHistory } from 'vue-router'
import LoginView from '@/views/LoginView'
import RegisterView from '@/views/RegisterView'
import verifyEmail from '@/views/verifyEmail'
import forgotPassword from '@/views/forgotPassword'
import resetPassword from '@/views/resetPassword'
import Home from '@/views/Home'
import Guard from '@/service/middleware'
import Profile from '@/views/Profile'
import Todo from '@/views/Todo'
import TodoTasks from '@/views/TodoTasks'

const routes = [
  {
    path: '/',
    beforeEnter: Guard.redirectIfNotAuthenticated,
    children:[
      {path: '', name: 'index', component: Home},
      {path: 'perfil', name: 'profile', component: Profile},
    ],
  },

  {
    path: '/login',
    name: 'login',
    beforeEnter: Guard.redirectIfAuthenticated,
    component: LoginView
  },

  {
    path: '/todo',
    beforeEnter: Guard.redirectIfNotAuthenticated,
    name: 'todo',
    component: Todo
  },

  {
    path: '/todo/:id',
    beforeEnter: Guard.redirectIfNotAuthenticated,
    name: 'todo-tasks',
    component: TodoTasks
  },

  {
    path: '/registro',
    name: 'register',
    component: RegisterView
  },

  {
    path: '/verificar-email',
    name: 'verifyEmail',
    component: verifyEmail
  },

  {
    path: '/esqueci-senha',
    name: 'forgotPassword',
    component: forgotPassword
  },

  {
    path: '/recuperar-senha',
    name: 'resetPassword',
    component: resetPassword
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router

import { createApp } from 'vue';
import App from '@/App.vue';
import router from '@/router';
import store from '@/store';

import '@/css/tailwind.css';
import CKEditor from '@ckeditor/ckeditor5-vue';
import axios from 'axios';
import Cookie from '@/service/cookie';

axios.defaults.baseURL = 'http://api.todolist.test/api';
axios.defaults.headers['Contente-Type'] = 'application/json';

axios.interceptors.request.use(function(config){
    const token = Cookie.getToken();

    if (token) {
        config.headers.common['Authorization'] = token;
    }

    return config;
});


const app = createApp(App);
app.config.globalProperties.$axios = axios;
app.use(store).use( CKEditor ).use(router).mount('#app');


